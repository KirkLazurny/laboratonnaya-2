#include "matr.h"
#include <iostream>
#include <stdlib.h>

int main()
{
    Matrix <int> Obj1;

    Obj1.AddColumn();
    Obj1.AddColumn();
    Obj1.AddColumn();//добавление столбца
    Obj1.AddRow();//добавление строки
    Obj1.AddRow();
    Obj1.AddRow();
    Obj1.DeleteColumn();
    Obj1.DeleteRow();

    Obj1.Insert(1, 1, 2);
    Obj1.Insert(2, 1, 3);
    Obj1.Insert(1, 2, 4);
    Obj1.Insert(2, 2, 5);
    Obj1.Insert(3, 3, 21);
    Obj1.Print();

    Obj1.Square(); //возведение в квадрат
    Obj1.Print();

    Obj1.Transposing(); //транспонирование
    Obj1.Print();

    Obj1.Multiplication(5);
    Obj1.Print();

    Obj1.Sled();

    Obj1.GetSubMatrix(1,1,2,2);

    Obj1.Clear(0);
    Obj1.Print();

    system("pause");
    return 0;
}



//Добавление/удаление столбца +/+
//Добавление/удаление строки +/+
//Задать/изменить определенный элемент +
//Очистка матрицы +
//Получение подматрицы +
//Получение определителя матрицы
//Транспонирование матрицы +
//Арифметические операции с матрицами (в доп)
//Вывод матрицы +

//DOPOLNITELNO:
//Квадрат матрицы +
//Умножение на число +
//След +

