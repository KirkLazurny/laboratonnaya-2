QT += core
QT -= gui

CONFIG += c++11

TARGET = Lab2
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

HEADERS += \
    matr.h
