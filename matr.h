#ifndef MATR_H
#define MATR_H

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

template <class Type>
class Matrix
{
    private:
        Type **Mtrx;
        int Column; //столбец
        int Row; //строка

    public:
        Matrix();
        void Print();
        void AddColumn();
        void DeleteColumn();
        void AddRow();
        void DeleteRow();
        void Insert(int,int,Type);
        void Transposing();
        void Square();
        void Multiplication(Type);
        void GetSubMatrix(int,int,int,int);
        void Clear(Type);
        Type Sled();
        ~Matrix();
};

template <typename Type>
Matrix<Type>::Matrix()
{
    Row=Column=1;
    Mtrx = new Type *[Row];
    for (int i = 0; i<Row;i++) Mtrx[i]=new Type[Column];
    Mtrx[0][0]=0;
}

template <typename Type>
void Matrix<Type>::AddColumn()
{
    Column++;
    Type **AddColumnMtrx = new Type *[Row];
    for (int i = 0; i<Row;i++) AddColumnMtrx[i]=new Type[Column];

    for (int count_row = 0; count_row<Row;count_row++)
    {
        for(int count_column = 0; count_column<Column;count_column++)
        {
           AddColumnMtrx[count_row][count_column] = Mtrx[count_row][count_column];
       }
    }

    for (int w = 0; w<Row; w++) AddColumnMtrx[w][Column-1]=0;

    for(int i=0;i<Row;i++)
    {
        delete [] Mtrx[i];
    }
    delete [] Mtrx;
    Mtrx = AddColumnMtrx;
}


template <typename Type>
void Matrix<Type>::DeleteColumn()
{
    Column--;
    Type **DeleteColumnMtrx = new Type*[Row];
    for (int i = 0; i<Row;i++) DeleteColumnMtrx[i]=new Type[Column];

    for (int count_row = 0; count_row<Row;count_row++)
    {
        for(int count_column = 0; count_column<Column;count_column++)
        {
           DeleteColumnMtrx[count_row][count_column] = Mtrx[count_row][count_column];
       }
    }

    for(int i=0;i<Row;i++)
    {
        delete [] Mtrx[i];
    }
    delete [] Mtrx;
    Mtrx = DeleteColumnMtrx;
}


template <typename Type>
void Matrix<Type>::AddRow()
{
    Row++;
    Type **AddRowMtrx = new Type*[Row];
    for (int i = 0; i<Row; i++) AddRowMtrx[i] = new Type[Column];

    for (int count_row = 0; count_row<Row-1; count_row++)
    {
        for (int count_column = 0; count_column<Column; count_column++)
        {
            AddRowMtrx[count_row][count_column] = Mtrx[count_row][count_column];
        }
    }

    for (int w = 0; w<Column; w++) AddRowMtrx[Row - 1][w] = 0;



    for (int i = 0; i<Row-1; i++)
    {
        delete[] Mtrx[i];
    }
    delete[] Mtrx;
    Mtrx = AddRowMtrx;
}


template <typename Type>
void Matrix<Type>::DeleteRow()
{
    Row--;
    Type **DeleteRowMtrx = new Type*[Row];
    for (int i = 0; i<Row;i++) DeleteRowMtrx[i]=new Type[Column];

    for (int count_row = 0; count_row<Row;count_row++)
    {
        for(int count_column = 0; count_column<Column;count_column++)
        {
            DeleteRowMtrx[count_row][count_column] = Mtrx[count_row][count_column];
        }
    }

    for(int i=0;i<Row;i++)
    {
        delete [] Mtrx[i];
    }
    delete [] Mtrx;
    Mtrx = DeleteRowMtrx;
}

template <typename Type>
void Matrix<Type>::Insert(int tRow, int tColumn, Type Value)
{
    if (tRow<=Row && tColumn<=Column) Mtrx[tRow-1][tColumn-1]=Value;
}

template <typename Type>
void Matrix<Type>::GetSubMatrix(int BeginRow,int StartColumn,int EndRow,int EndColumn)
{
    cout<<endl;
    for (int i=BeginRow-1; i<EndRow;i++)
    {
        for (int j=StartColumn-1; j<EndColumn; j++)
        {
         cout<<Mtrx[i][j]<<" ";
        }
        cout<<endl;
    }
}

template <typename Type>
void Matrix<Type>::Transposing()
{
    int Tempo = Row;
    Row = Column;
    Column = Tempo;

    Type **TranspMtrx = new Type*[Row];
    for (int i = 0; i<Row; i++) TranspMtrx[i] = new Type[Column];
    for (int i = 0; i < Row ; i++)
    {
        for (int j = 0; j<Column; j++)
        {
            TranspMtrx[i][j] = Mtrx[j][i];
        }
    }
    for (int i = 0; i<Column - 1; i++)
    {
        delete[] Mtrx[i];
    }
    delete[] Mtrx;
    Mtrx = TranspMtrx;

}

template <typename Type>
void Matrix<Type>::Print()
{
    for (int i = 0; i <Row; i++)
    {
        cout<<endl;
        for (int j=0; j<Column;j++)
        {
            cout<<Mtrx[i][j]<<" ";
        }
    }
    cout<<endl;
}

template <typename Type>
void Matrix<Type>::Square()
{
    Type **SquareMtrx = new Type*[Row];
    for (int v = 0; v<Row; v++) SquareMtrx[v] = new Type[Column];

    if (Row == Column)
    {
        for (int count_row = 0; count_row<Row; count_row++)
        {
            for (int count_column = 0; count_column<Column; count_column++)
            {
                int j = 0;
                SquareMtrx[count_row][count_column] = 0;
                for (int i = 0; i<Row; i++)
                {
                    SquareMtrx[count_row][count_column] = SquareMtrx[count_row][count_column] + Mtrx[count_column][j] * Mtrx[i][count_row];
                    j++;
                }
            }
        }

    }

    for (int dl = 0; dl<Column - 1; dl++)
    {
        delete[] Mtrx[dl];
    }
        delete[] Mtrx;
        Mtrx = SquareMtrx;
}



template <typename Type>
void Matrix<Type>::Multiplication(Type Number)
{
    for (int i = 0; i < Row ; i++)
    {
        for (int j = 0; j<Column; j++)
        {
            Mtrx[i][j]=Mtrx[i][j]*Number;
        }
    }
}


template <typename Type>
Type Matrix<Type>::Sled()
{
    Type SledValue = 0;
    if (Row==Column)
    {
        for (int i = 0; i < Row ; i++)
        {
            SledValue=SledValue+Mtrx[i][i];
        }
    }
    return SledValue;
}

template <typename Type>
void Matrix<Type>::Clear(Type Value)
{
    for (int i = 0; i < Row ; i++)
    {
        for (int j = 0; j<Column; j++)
        {
            Mtrx[i][j]=Value;
        }
    }
}

template <typename Type>
Matrix<Type>::~Matrix()
{
    for(int i=0;i<Row;i++)
    {
        delete [] Mtrx[i];
    }
    delete [] Mtrx;
}
#endif // MATR_H
